package carbook.exceptions

import carbook.data.Car

sealed abstract class CarsBookExceptions(message: String) extends Exception(message)

final case class CarAlreadyExistException(car: Car)
  extends CarsBookExceptions(s"$car is already exist")

final case class CarIsNotExistException(number: String)
  extends CarsBookExceptions(s"car with number - $number is not exist")

final case class CarNumberInvalidException(number: String)
  extends CarsBookExceptions(s"invalid car number - $number")