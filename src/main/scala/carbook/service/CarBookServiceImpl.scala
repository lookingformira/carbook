package carbook.service

import carbook.data.{Car, CarJson, Stat}
import carbook.exceptions.{CarIsNotExistException, CarNumberInvalidException}
import carbook.storage.DataBase
import io.circe.Json
import io.circe.syntax._

import scala.concurrent.{ExecutionContext, Future}

class CarBookServiceImpl(implicit val db: DataBase[Future], implicit val ec: ExecutionContext) extends CarBookService[Future] {

  override def getCars: Future[Json] = db.selectAll.map(seq => seq.map(convertCars).asJson)

  override def getCarByNumber(number: String): Future[Json] = db.find(number).map {
    case Some(car) => convertCars(car).asJson
    case None => CarIsNotExistException(number).getMessage.asJson
  }

  override def getCarsByParameters(model: String, color: String, prodDate: Int): Future[Json] =
    db.find(model, color, prodDate).map(seq => seq.map(convertCars).asJson)

  override def insert(car: Car): Future[String] = {
    checkCarNumber(car.number) flatMap {
      case true => db.insert(car) flatMap(_ => Future.successful("inserting complete"))
      case false => Future.failed(CarNumberInvalidException(car.number))
    }
  }

  override def delete(number: String): Future[String] = db.delete(number) flatMap(_ => Future.successful("deleting complete"))

  override def getStat: Future[Json] =
    for {
      count <- db.countTable
      max <- db.getDateMax
      min <- db.getDateMin
    } yield Stat(count, min.headOption, max.headOption).asJson

  def convertCars(car: Car): CarJson = CarJson(car.number, car.model, car.color, car.prodYear)

  def checkCarNumber(number: String): Future[Boolean] = {
    val pattern = "([АВЕКМНОРСТУХ])([0-9]{3})([АВЕКМНОРСТУХ]{2})([0-9]{2,3})".r
    Future.successful(pattern.matches(number))
  }

}
