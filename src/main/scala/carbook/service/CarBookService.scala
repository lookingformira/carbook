package carbook.service

import carbook.data.Car
import io.circe.Json

trait CarBookService[F[_]] {

  /**
   * Получение списка всех автомобилей
   *
   * @return json с авто
   */
  def getCars: F[Json]

  /**
   * Поиск авто по его номеру
   *
   * @param number номер автомобиля для поиска
   * @return json с найденным авто
   */
  def getCarByNumber(number: String): F[Json]

  /**
   * Поиск автомобилей по параметрам
   *
   * @param model    модель авто
   * @param color    цвет авто
   * @param prodDate дата производства авто
   * @return json со списком автомобилей
   */
  def getCarsByParameters(model: String, color: String, prodDate: Int): F[Json]

  /**
   * Добавления автомобиля в БД
   *
   * @param car который необходимо добавить
   * @return сообщение об успешном добавлении
   */
  def insert(car: Car): F[String]

  /**
   * Удаляет автомобиль из БД
   *
   * @param number номер автомобиля для удаления
   * @return сообщение об успешном удалении
   */
  def delete(number: String): F[String]

  /**
   * Получение статистики
   *
   * @return json со статистикой по БД
   */
  def getStat: F[Json]

}
