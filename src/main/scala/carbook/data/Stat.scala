package carbook.data

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

import java.sql.Timestamp
import java.text.SimpleDateFormat

case class Stat(amount: Int, firstNote: Option[Timestamp], lastNote: Option[Timestamp])

object Stat {

  implicit val TimestampFormat: Encoder[Timestamp] = (a: Timestamp) =>
    Encoder.encodeString.apply(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(a))

  implicit val statEncoder: Encoder[Stat] = deriveEncoder[Stat]

}
