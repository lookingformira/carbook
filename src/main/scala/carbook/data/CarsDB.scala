package carbook.data

import slick.jdbc.PostgresProfile.api._

import java.sql.Timestamp
import java.time.LocalDateTime

case class Car(number: String,
               model: String,
               color: String,
               prodYear: Int,
               addDate: Timestamp = Timestamp.valueOf(LocalDateTime.now()),
               id: Option[Int] = Some(0)
              )

class CarsTable(tag: Tag) extends Table[Car](tag, "CarsTable") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def addDate = column[Timestamp]("addDate")

  def number = column[String]("number")

  def model = column[String]("model")

  def color = column[String]("color")

  def prodYear = column[Int]("prodYear")

  override def * = (number, model, color, prodYear, addDate, id.?) <> (Car.tupled, Car.unapply)
}




