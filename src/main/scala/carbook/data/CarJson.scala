package carbook.data

import cats.effect.IO
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.http4s.EntityDecoder
import org.http4s.circe.jsonOf

case class CarJson(number: String,
                   model: String,
                   color: String,
                   prodYear: Int)

object CarJson {
  implicit val carsEncoder: Encoder[CarJson] = deriveEncoder[CarJson]
  implicit val carsDecoder: Decoder[CarJson] = deriveDecoder[CarJson]
  implicit val decoder: EntityDecoder[IO, CarJson] = jsonOf[IO, CarJson]
}
