package carbook.storage

import carbook.data.Car

import java.sql.Timestamp

trait DataBase[F[_]] {

  /**
   * Создает таблицу
   *
   * @return unit
   */
  def createTable: F[Unit]

  /**
   * Запрос на получение всех записей
   *
   * @return коллекция автомобилей
   */
  def selectAll: F[Seq[Car]]

  /**
   * Добавления автомобиля в БД
   *
   * @param car который необходимо добавить
   * @return единицу, если запись добавлена
   */
  def insert(car: Car): F[Int]

  /**
   * Удаляет автомобиль из БД
   *
   * @param number номер автомобиля для удаления
   * @return единицу, если запись удалена
   */
  def delete(number: String): F[Int]

  /**
   * Поиск в БД по номеру автомобиля
   *
   * @param number номер автомобиля для поиска
   * @return опциональное значение автомобиля
   */
  def find(number: String): F[Option[Car]]

  /**
   * Поиск в БД по параметрам автомобиля
   *
   * @param model    модель авто
   * @param color    цвет авто
   * @param prodDate дата производства авто
   * @return коллекция автомобилей
   */
  def find(model: String, color: String, prodDate: Int): F[Seq[Car]]

  /**
   * Запрос для определения даты первой записи
   *
   * @return коллекция дат
   */
  def getDateMin: F[Seq[Timestamp]]


  /**
   * Запрос для определения даты последней записи
   *
   * @return коллекция дат
   */
  def getDateMax: F[Seq[Timestamp]]


  /**
   * Запрос для определения колечества записей в БД
   *
   * @return количество записей
   */
  def countTable: F[Int]

}



