package carbook.storage

import carbook.data.{Car, CarsTable}
import carbook.exceptions.{CarAlreadyExistException, CarIsNotExistException}
import slick.jdbc.PostgresProfile.api._

import java.sql.Timestamp
import scala.concurrent.{ExecutionContext, Future}

case class DataBaseImpl(config: String)(implicit val ec: ExecutionContext) extends DataBase[Future] {

  val db = Database.forConfig(config)

  val cars = TableQuery[CarsTable]

  def dropTable: Future[Unit] = db.run(cars.schema.dropIfExists)

  override def createTable: Future[Unit] = db.run(cars.schema.createIfNotExists)

  override def selectAll: Future[Seq[Car]] = db.run(cars.sortBy(car => (car.model, car.prodYear)).result)

  override def insert(car: Car): Future[Int] =
    find(car.number).flatMap {
      case Some(car) => Future.failed(CarAlreadyExistException(car))
      case None => db.run(cars += car)
    }

  override def delete(number: String): Future[Int] =
    find(number).flatMap {
      case Some(_) =>
        val query = for {
          car <- cars if car.number like number
        } yield car
        db.run(query.delete)
      case None => Future.failed(CarIsNotExistException(number))

    }

  override def find(number: String): Future[Option[Car]] = {
    val query = for {
      car <- cars if car.number like number
    } yield car
    db.run(query.result.headOption)
  }

  override def find(model: String, color: String, prodDate: Int): Future[Seq[Car]] = {
    val query = for {
      car <- cars if (car.color like color) &&
        (car.model like model) &&
        (car.prodYear === prodDate)
    } yield car
    db.run(query.result)
  }

  override def getDateMin: Future[Seq[Timestamp]] = db.run(cars.sortBy(_.addDate.asc).map(_.addDate).take(1).result)

  override def getDateMax: Future[Seq[Timestamp]] = db.run(cars.sortBy(_.addDate.desc).map(_.addDate).take(1).result)

  override def countTable: Future[Int] = db.run(cars.length.result)

}
