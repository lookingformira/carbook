package carbook

import carbook.data.{Car, CarJson}
import carbook.service.CarBookServiceImpl
import carbook.storage.DataBase
import cats.data.Kleisli
import cats.effect.IO
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe._
import org.http4s.dsl.impl.MatrixVar
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.{HttpRoutes, Request, Response}

import scala.concurrent.{ExecutionContext, Future}

class Routes(implicit val ec: ExecutionContext, implicit val dataBase: DataBase[Future]) {

  val service = new CarBookServiceImpl

  def allRoutes: Kleisli[IO, Request[IO], Response[IO]] = HttpRoutes.of[IO] {

    case GET -> Root / "cars" =>
      Ok(IO.fromFuture(IO(service.getCars)))
        .handleErrorWith(e => BadRequest(e.getMessage))

    case GET -> Root / "cars" / ParametersExtractor(model, color, prodDate) =>
      Ok(IO.fromFuture(IO(service.getCarsByParameters(model, color, prodDate.toInt))))
        .handleErrorWith(e => BadRequest(e.getMessage))

    case GET -> Root / "cars" / NumberExtractor(number) =>
      Ok(IO.fromFuture(IO(service.getCarByNumber(number))))
        .handleErrorWith(e => BadRequest(e.getMessage))

    case GET -> Root / "cars" / "delete" / number =>
      Ok(IO.fromFuture(IO(service.delete(number))))
        .handleErrorWith(e => BadRequest(e.getMessage))

    case GET -> Root / "cars" / "stat" =>
      Ok(IO.fromFuture(IO(service.getStat)))
        .handleErrorWith(e => BadRequest(e.getMessage))

    case req@POST -> Root / "cars" / "add" =>
      for {
        car <- req.as[CarJson]
        action = IO.fromFuture(IO(service.insert(Car(car.number, car.model, car.color, car.prodYear))))
        resp <- Ok(action).handleErrorWith(e => BadRequest(e.getMessage))
      } yield resp

    case GET -> Root / "echo" / name =>
      Ok(s"hello, $name")
  }.orNotFound

}

object ParametersExtractor extends MatrixVar("parameters", List("model", "color", "prodDate"))

object NumberExtractor extends MatrixVar("number", List("value"))
