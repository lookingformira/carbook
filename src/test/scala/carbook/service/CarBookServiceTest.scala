package carbook.service

import carbook.data.Car
import carbook.exceptions.{CarAlreadyExistException, CarIsNotExistException, CarNumberInvalidException}
import carbook.storage.DataBaseImpl
import io.circe.Json
import org.scalatest.flatspec.AsyncFlatSpec

class CarBookServiceTest extends AsyncFlatSpec {

  implicit val dataBase: DataBaseImpl = DataBaseImpl("h2")

  val service = new CarBookServiceImpl

  behavior of "getCars"

  it should "return cars in json" in {

    val json = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable

      json <- service.getCars
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))

  }

  behavior of "getCarByNumber"

  it should "return json" in {

    val json = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(Car("aaa", "audi", "blue", 1974))
      json <- service.getCarByNumber("aaa")
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))

  }

  behavior of "getCarsByParameters"

  it should "return cars in json" in {
    val json = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(Car("aaa", "audi", "blue", 1974))
      json <- service.getCarsByParameters("audi", "blue", 1974)
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))
  }

  behavior of "insert"

  it should "insert car in db" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      str <- service.insert(Car("А122АВ705", "audi", "blue", 1974))
    } yield str

    future.map(str => assert(str == "inserting complete"))
  }

  it should "throw CarNumberInvalidException exception" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      str <- service.insert(Car("aaa", "audi", "blue", 1974))
    } yield str

    recoverToSucceededIf[CarNumberInvalidException](future)
  }

  it should "throw CarAlreadyExistException exception" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- service.insert(Car("А122АВ705", "audi", "blue", 1974))
      str <- service.insert(Car("А122АВ705", "audi", "blue", 1974))
    } yield str

    recoverToSucceededIf[CarAlreadyExistException](future)
  }

  behavior of "delete"

  it should "delete car from db" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- service.insert(Car("А122АВ705", "audi", "blue", 1974))
      str <- service.delete("А122АВ705")
    } yield str

    future.map(str => assert(str == "deleting complete"))
  }

  it should "throw exception" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      str <- service.delete("А122АВ705")
    } yield str

    recoverToSucceededIf[CarIsNotExistException](future)
  }

  behavior of "getStat"

  it should "return Json" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      json <- service.getStat
    } yield json

    future.map(json => assert(json.isInstanceOf[Json]))
  }





}
