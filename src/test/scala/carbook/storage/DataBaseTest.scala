package carbook.storage

import carbook.data.Car
import carbook.exceptions.{CarAlreadyExistException, CarIsNotExistException}
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

import java.sql.Timestamp

class DataBaseTest extends AsyncFlatSpec with Matchers{

  val dataBase: DataBaseImpl = DataBaseImpl("h2")

  behavior of "createTable"

  it should "create table" in {

    dataBase.dropTable
    dataBase.createTable

    dataBase.selectAll.map(seq => assert(seq.isEmpty))
  }

  behavior of "insert"

  it should "add car to table" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      cars <- dataBase.selectAll
    } yield cars
    future.map(cars => assert(cars.size == 1))
  }

  it should "throw already exist exception" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      _ <- dataBase.insert(car)
      cars <- dataBase.selectAll
    } yield cars

    recoverToSucceededIf[CarAlreadyExistException](future)
  }

  behavior of "delete"

  it should "delete car from table" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      _ <- dataBase.delete("aaa")
      cars <- dataBase.selectAll
    } yield cars
    future.map(cars => assert(cars.isEmpty))
  }

  it should "throw exception - car with number is not exist" in {
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.delete("ooo")
      cars <- dataBase.selectAll
    } yield cars
    recoverToSucceededIf[CarIsNotExistException](future)
  }

  behavior of "find"

  it should "find car by number" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      cars <- dataBase.find("aaa")
    } yield cars
    future.map(cars => assert(cars.size == 1))
  }

  it should "find car by multiply parameters" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      cars <- dataBase.find("audi", "blue", 1974)
    } yield cars
    future.map(cars => assert(cars.size == 1))
  }

  behavior of "getDateMin"

  it should "return min date" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      seq <- dataBase.getDateMin
    } yield seq
    future.map(seq => assert(seq.size <= 1 && seq.isInstanceOf[Seq[Timestamp]]))
  }

  behavior of "getDateMax"

  it should "return max date" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      seq <- dataBase.getDateMax
    } yield seq
    future.map(seq => assert(seq.size <= 1 && seq.isInstanceOf[Seq[Timestamp]]))
  }

  behavior of "countTable"

  it should "count amount cars in table" in {
    val car = Car("aaa", "audi", "blue", 1974)
    val future = for {
      _ <- dataBase.dropTable
      _ <- dataBase.createTable
      _ <- dataBase.insert(car)
      seq <- dataBase.countTable
    } yield seq
    future.map(amount => assert(amount == 1))
  }

}
